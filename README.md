# Goodbye 2

Clone with HTTPS:
```shell
git clone --recurse-submodules https://gitlab.com/eternalfest/games/goodbye2.git
```

Clone with SSH:
```shell
git clone --recurse-submodules git@gitlab.com:eternalfest/games/goodbye2.git
```
