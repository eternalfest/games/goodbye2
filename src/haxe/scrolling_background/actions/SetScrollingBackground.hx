package scrolling_background.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class SetScrollingBackground implements IAction {
    public var name(default, null): String = Obfu.raw("scrolling_background");
    public var isVerbose(default, null): Bool = false;

    private var mod: ScrollingBackground;

    public function new(mod: ScrollingBackground) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();

        this.mod.scrollingBackground(game);

        return false;
    }
}
