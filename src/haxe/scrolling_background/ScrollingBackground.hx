package scrolling_background;

import scrolling_background.actions.SetScrollingBackground;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import etwin.flash.Key;
import patchman.Ref;
import patchman.module.Run;
import ef.api.run.IRun;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;


@:build(patchman.Build.di())
class ScrollingBackground {

    @:diExport
    public var action(default, null): IAction;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var phases = [];
    public var index = 0;

    public function new() {
        this.action = new SetScrollingBackground(this);

        var patches = [
            Ref.auto(hf.mode.GameMode.getControls).after(function(hf: hf.Hf, self: hf.mode.GameMode) {
                if (Key.isDown(75) && index < phases.length - 3) {
                    index = phases.length - 3;
                }
            }),
            Ref.auto(hf.mode.GameMode.holeUpdate).after(function(hf: hf.Hf, self: hf.mode.GameMode) {
                while(index < phases.length) {
                    if(phases[index](self)) {
                        index++;
                    }
                    else
                        break;
                }
            }),
        ];

        this.patches = FrozenArray.from(patches);
    }

    public function scrollingBackground(game: hf.mode.GameMode): Void {
        var save = game.root.Data.PLAYER_SPEED;
        var reduction = game.root.GameManager.CONFIG.hasOption(game.root.Data.OPT_BOOST) ? 0.28 : 0.28;
        var mult = game.root.GameManager.CONFIG.hasOption(game.root.Data.OPT_BOOST) ? 17.39 : 17.5;
        var base = game.root.GameManager.CONFIG.hasOption(game.root.Data.OPT_BOOST) ? 5.08 : 3.9;
        game.root.Data.PLAYER_SPEED = 0;// reduction;
        var bottom: Array<Dynamic> = [];
        var x = 0;
        var p = game.getPlayerList()[0];
        p.hide();
        p.changeWeapon(game.root.Data.WEAPON_NONE);
        var curDarkness = Math.round(0);
        var targetDarkness = Math.round(1000);

        var backgrounds = ['rolling_background', 'rolling_background2'];
        var day = function(game: hf.mode.GameMode) {
            (cast game).back.removeMovieClip();
            (cast game).back = sprite(game, 0, backgrounds[0], true);
            return true;
        }
        var light = function(game: hf.mode.GameMode) {
            if (curDarkness < targetDarkness) {
                curDarkness += 1;
                if (curDarkness > targetDarkness) {
                    curDarkness = targetDarkness;
                }
            }
            if (curDarkness > targetDarkness) {
                curDarkness -= 1;
                if (curDarkness < targetDarkness) {
                    curDarkness = targetDarkness;
                }
            }
            game.world.darknessFactor = curDarkness;
            game.darknessMC._alpha = curDarkness;
        }
        var transition3 = function(game: hf.mode.GameMode) {
            curDarkness = 100;
            targetDarkness = 0;
            for(i in bottom) {
                i.removeMovieClip();
            }
            bottom = new Array();
            p.filters = [ColorMatrix.fromHsv(150, 1, -1).toFilter()];
            p.show();
            return true;
        }

        var phase2 = function(game: hf.mode.GameMode) {
            game.darknessMC.holes[0]._visible = false;
            light(game);
            var player = game.getPlayerList()[0];
            if(game.root.Data.PLAYER_SPEED == 0 /*reduction*/) {
                if(game.root.Data.PLAYER_SPEED == 0 && player.lockTimer <= 0 /*reduction*/) {
                    if(bottom.length == 0){
                        bottom.push(sprite(game, -420, Obfu.raw('ground'), false));
                        bottom.push(sprite(game, 0, Obfu.raw('ground'), false));
                        bottom.push(sprite(game, 420, Obfu.raw('ground'), false));
                        bottom.push(sprite(game, 420 * 2, Obfu.raw('ground'), false));
                        bottom.push(sprite(game, 420 * 3, Obfu.raw('ground'), false));
                        bottom.push(sprite(game, 420 * 4, Obfu.raw('ground_tree'), false));
                        bottom.push(sprite(game, 420 * 5, Obfu.raw('ground_tree4'), false));
                        bottom.push(sprite(game, 420 * 4, Obfu.raw('ground_tree2'), false));
                        bottom.push(sprite(game, 420 * 7, Obfu.raw('ground_tree'), false));
                        bottom.push(sprite(game, 420 * 8, Obfu.raw('ground_rip'), false));
                        bottom.push(sprite(game, 420 * 9, Obfu.raw(''), false));
                        bottom.push(sprite(game, 420 * 10, Obfu.raw(''), false));
                    }
                    //var dx = player.dx * mult;
                    //var dir = dx < 0 ? 1 : -1;
                    var dir: Float = Key.isDown(player.ctrl.left) ? 1 : Key.isDown(player.ctrl.right) ? -1 : 0;
                    var dx: Float = dir;
                    if(dx != 0) {
                        var tmp_x = player.x - dir * (reduction * player.speedFactor);
                        if(tmp_x > 10.028)
                        {
                            player.x = tmp_x;
                            dx = base * dir;
                            dx = (player.x - 10) * mult;
                            for(i in 0...bottom.length - 1) {
                                //if((dir == -1 && bottom[i]._x > 420 * 2) || (dir == 1 && bottom[i]._x < 420))
                                //	bottom[i]._x += 2 * 420 * dir;
                                bottom[i]._x = ((-dx) + i * 420);
                            }
                            bottom[bottom.length - 1]._x = ((-dx) + (bottom.length - 2) * 420);
                        }
                    }
                }
                else {
                    game.root.Data.PLAYER_SPEED = 0 /*reduction*/;
                }
            }
            else {
                var offset = bottom[5]._x;
                for(i in bottom) {
                    bottom[i]._x -= offset;
                }
                //bottom[5]._x = 0;
                //bottom[6]._x = 0;
                game.root.Data.PLAYER_SPEED = save;
                return true;
            }
            return false;
        }

        phases = [transition3, day, phase2];
    }

    public function sprite(game: hf.mode.GameMode, x: Float, name: String, back): Dynamic {
        var v33 = x;
        var v34 = 0;
        var v35 = 1;
        var v37 = name;
        var v39 = v33;
        var v40 = v34;
        v39 = game.flipCoordReal(v39);
        if (game.fl_mirror) {
            v39 += game.root.Data.CASE_WIDTH;
        }
        var v41: Dynamic = game.world.view.attachSprite(v37, v39, v40, back);
        if (game.fl_mirror) {
            v41._xscale *= -1;
        }
        v41.stop();
        (cast v41).sub.stop();
        game.world.scriptEngine.mcList.push({sid: v35, mc: v41});
        return v41;
    }

}
