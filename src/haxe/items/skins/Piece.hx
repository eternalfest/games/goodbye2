package items.skins;

import vault.ISkin;
import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;


class Piece implements ISkin {
    public static var ID_PIECE: Int = 51;

    public var id(default, null): Int = 1239;
    public var sprite(default, null): Null<String> = null;

    public function new() {}

    public function skin(mc: etwin.flash.MovieClip, subId: Null<Int>): Void {
        mc.gotoAndStop("" + (ID_PIECE + 1));

        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(52, 1.5, 0.7).toFilter();
        mc.filters = [filter];

    }
}
