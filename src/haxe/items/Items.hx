package items;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.IItem;
import vault.ISpec;
import vault.ISkin;

import items.skins.Piece;

@:build(patchman.Build.di())
class Items {
    @:diExport
    public var skin(default, null): ISkin;

    public function new() {

        this.skin =  new Piece();
    }
}
