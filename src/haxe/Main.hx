import bugfix.Bugfix;
import debug.Debug;
import merlin.Merlin;
import game_params.GameParams;
import better_script.NoNextLevel;
import better_script.Players;
import better_script.Debug;
import maxmods.VisualEffects;
import maxmods.Misc;
import patchman.IPatch;
import patchman.Patchman;
import hf.Hf;
import goodbye.Goodbye;
import goodbye.DarknessHaloPatches;
import disabling.Disabling;
import player_state.PlayerState;
import vault.Vault;
import items.Items;
import scrolling_background.ScrollingBackground;


@:build(patchman.Build.di())
class Main {
    public static function main(): Void {
        Patchman.bootstrap(Main);
    }

    public function new(
        bugfix: Bugfix,
        debug: debug.Debug,
        gameParams: GameParams,
        noNextLevel: NoNextLevel,
        players: Players,
        bsDebug: better_script.Debug,
        visualEffects: VisualEffects,
        misc: Misc,
        goodbye: Goodbye,
        scrolling_background: ScrollingBackground,
        items: Items,
        vault: Vault,
        darknessHaloPatches: DarknessHaloPatches,
        disabling: Disabling,
        player_state: PlayerState,
        merlin: Merlin,
        patches: Array<IPatch>,
        hf: Hf
    ) {
        Patchman.patchAll(patches, hf);
    }
}
