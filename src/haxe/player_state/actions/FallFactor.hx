package player_state.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class FallFactor implements IAction {
    public var name(default, null): String = Obfu.raw("fallFactor");
    public var isVerbose(default, null): Bool = false;

    private var mod: PlayerState;

    public function new(mod: PlayerState) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var n: Float = ctx.getFloat(Obfu.raw("n"));

        this.mod.fallFactor(game, n);

        return false;
    }
}
