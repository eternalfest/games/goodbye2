package player_state.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class DisableLeft implements IAction {
    public var name(default, null): String = Obfu.raw("disableLeft");
    public var isVerbose(default, null): Bool = false;

    private var mod: PlayerState;

    public function new(mod: PlayerState) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var n: Bool = ctx.getBool(Obfu.raw("n"));

        this.mod.disableLeft(game, n);

        return false;
    }
}
