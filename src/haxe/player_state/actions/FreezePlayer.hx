package player_state.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class FreezePlayer implements IAction {
    public var name(default, null): String = Obfu.raw("freezePlayer");
    public var isVerbose(default, null): Bool = false;

    private var mod: PlayerState;

    public function new(mod: PlayerState) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var t: Float = ctx.getFloat(Obfu.raw("t"));

        this.mod.freezePlayer(game, t);

        return false;
    }
}
