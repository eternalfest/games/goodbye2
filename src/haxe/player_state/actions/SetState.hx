package player_state.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class SetState implements IAction {
    public var name(default, null): String = Obfu.raw("setState");
    public var isVerbose(default, null): Bool = false;

    private var mod: PlayerState;

    public function new(mod: PlayerState) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var n: String = ctx.getOptString(Obfu.raw("n")).or("");

        this.mod.setState(game, n);

        return false;
    }
}
