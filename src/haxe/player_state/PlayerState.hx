package player_state;

import patchman.DebugConsole;
import player_state.actions.FreezePlayer;
import player_state.actions.SetSpeed;
import player_state.actions.SetState;
import player_state.actions.DisableLeft;
import player_state.actions.FallFactor;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import etwin.flash.Key;
import patchman.Ref;

@:build(patchman.Build.di())
class PlayerState {

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new() {
        this.actions = FrozenArray.of(
            new FreezePlayer(this),
            new SetSpeed(this),
            new SetState(this),
            new DisableLeft(this),
            new FallFactor(this)
        );

        var patches = [

            Ref.auto(hf.entity.Player.killPlayer).before(function(hf: hf.Hf, self: hf.entity.Player) {
                self.game.root.Data.SHIELD_DURATION = 0;
                if (self.lives <= 0) {
                    self.lives = 1;
                }
            }),

        ];

        this.patches = FrozenArray.from(patches);
    }

    public function freezePlayer(game: hf.mode.GameMode, t: Float): Void {
        var playerList = game.getPlayerList();
        for (player in playerList) {
            player.lockControls(t - 1);
            player.dx = 0;
        }
    }

    public function setSpeed(game: hf.mode.GameMode, n: Float): Void {
        var playerList = game.getPlayerList();
        for (player in playerList) {
            player.speedFactor = n;
        }
    }

    public function setState(game: hf.mode.GameMode, n: String): Void {
        var playerList = game.getPlayerList();
        for (player in playerList) {
            if (n == Obfu.raw("sad")) {
                player.setBaseAnims(game.root.Data.ANIM_PLAYER_WALK_L, game.root.Data.ANIM_PLAYER_STOP_L);
            }
            else if (n == Obfu.raw("freeze")) {
                player.setBaseAnims(game.root.Data.ANIM_PLAYER_STOP, game.root.Data.ANIM_PLAYER_STOP);
            }
            else {
                player.setBaseAnims(game.root.Data.ANIM_PLAYER_WALK, game.root.Data.ANIM_PLAYER_STOP);
            }
        }
    }

    public function disableLeft(game: hf.mode.GameMode, n: Bool): Void {
        game.getPlayerList()[0].ctrl.setKeys(Key.UP, Key.DOWN, (n ? 999 : Key.LEFT), Key.RIGHT, Key.SPACE);
    }

    public function fallFactor(game: hf.mode.GameMode, n: Float): Void {
        game.getPlayerList()[0].fallFactor = n;
    }

}

