package goodbye.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class StopSound implements IAction {
    public var name(default, null): String = Obfu.raw("stopSound");
    public var isVerbose(default, null): Bool = false;

    private var mod: Goodbye;

    public function new(mod: Goodbye) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var n: String = ctx.getString(Obfu.raw("n"));

        this.mod.stopSound(game, n);

        return false;
    }
}
