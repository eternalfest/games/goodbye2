package goodbye.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class PosXR implements IAction {
    public var name(default, null): String = Obfu.raw("pos_xr");
    public var isVerbose(default, null): Bool = false;

    private var mod: Goodbye;

    public function new(mod: Goodbye) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var x: Float = ctx.getFloat(Obfu.raw("x"));

        if (game.getPlayerList()[0].x / 20 > x) {
            return true;
        }
        return false;
    }
}

class PosYR implements IAction {
    public var name(default, null): String = Obfu.raw("pos_yr");
    public var isVerbose(default, null): Bool = false;

    private var mod: Goodbye;

    public function new(mod: Goodbye) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var y: Float = ctx.getFloat(Obfu.raw("y"));

        if (game.getPlayerList()[0].y / 20 > y) {
            return true;
        }
        return false;
    }
}