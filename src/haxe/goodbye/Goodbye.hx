package goodbye;

import patchman.DebugConsole;
import goodbye.actions.Pause;
import goodbye.actions.RemoveFruits;
import goodbye.actions.CustomMsg;
import goodbye.actions.PosR;
import goodbye.actions.StopSound;
import goodbye.actions.Darkness2;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import etwin.flash.Key;
import patchman.Ref;
import patchman.module.Run;
import ef.api.run.IRun;


@:build(patchman.Build.di())
class Goodbye {

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public var isCustomPause: Bool = false;
    public var canResumeCustomPause: Bool = false;
    private var run: Run;


    public function new(run: Run) {
        this.run = run;

        this.actions = FrozenArray.of(
            new Pause(this),
            new RemoveFruits(this),
            new CustomMsg(this),
            new PosXR(this),
            new PosYR(this),
            new StopSound(this),
            new Darkness2()
        );

        var patches = [
            Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode) {
                if (this.isCustomPause && this.canResumeCustomPause && Key.isDown(Key.ENTER)) {
                    self.unlock();
                    self.world.unlock();
                    this.isCustomPause = false;
                }
            }),

            Ref.auto(hf.SoundManager.playSound).wrap(function(hf: hf.Hf, self: hf.SoundManager, name: String, chan: Int, old) {
                if (name == "sound_player_death") {
                    old(self, Obfu.raw("player_death"), chan);
                }
                else {
                    old(self, name, chan);
                }
            }),

            Ref.auto(hf.entity.bad.walker.Cerise.update).after(function(hf: hf.Hf, self: hf.entity.bad.walker.Cerise) {
                self.dir = 1;
            }),

            Ref.auto(hf.entity.Bad.onDeathLine).replace(function(hf: hf.Hf, self: hf.entity.Bad) {
                self.destroy();
            }),

            Ref.auto(hf.levels.GameMechanics.update).replace(function(hf: hf.Hf, self: hf.levels.GameMechanics) {
                Ref.call(hf, super.update, self);
                if (self.fl_parsing) {
                    self.parseCurrentIA(self._iteration);
                }
                if (!self.fl_lock) {
                    self.scriptEngine.update();
                }
                for (portal in self.portalList) {
                    portal.mc._y = portal.y + 3 * Math.sin(portal.cpt);
                    portal.cpt += hf.Timer.tmod * 0.1;
                }
            }),

        ];

        this.patches = FrozenArray.from(patches);
    }

    public function customPause(game: hf.mode.GameMode, canResume: Bool): Void {
        game.root.Data.WAIT_TIMER = 9999999999;
        game.fl_disguise = false;
        this.isCustomPause = true;
        this.canResumeCustomPause = canResume;
        game.lock();
        game.world.lock();
    }

    public function removeFruits(game: hf.mode.GameMode): Void {
        var badList = game.getBadClearList();
        for (bad in badList) {
            bad.destroy();
        }
    }

    public function customMsg(game: hf.mode.GameMode): Void {
        var getRun = this.run.getRun();
        game.attachPop('<p><font color="#8A0808">C\'est amusant de jouer avec toi, ' + getRun.user.displayName + '</font></p>', false);
    }

    public function stopSound(game: hf.mode.GameMode, n: String): Void {
        game.soundMan.stopSound(n, 7);
    }

}
