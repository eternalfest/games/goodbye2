package disabling;

import patchman.DebugConsole;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import etwin.flash.Key;
import patchman.Ref;

@:build(patchman.Build.di())
class Disabling {

    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    public function new() {

        var patches = [

            Ref.auto(hf.mode.GameMode.getControls).wrap(function(hf: hf.Hf, self: hf.mode.GameMode, old) {
                if (Key.isDown(27)) { // Esc
                    return;
                }
                if (Key.isDown(67)) { // C
                    return;
                }
                if (Key.isDown(80)) { // P
                    return;
                }
                if (Key.isDown(68)) { // D
                    return;
                }
                if (Key.isDown(77)) { // M
                    return;
                }
                if (Key.isDown(66)) { // B
                    return;
                }
                if (Key.isDown(70)) { // F
                    return;
                }
                old(self);
            }),

            Ref.auto(hf.mode.GameMode.onPause).replace(function(hf: hf.Hf, self: hf.mode.GameMode) {
                return;
            }),

            Ref.auto(hf.mode.GameMode.onMap).replace(function(hf: hf.Hf, self: hf.mode.GameMode) {
                return;
            }),

            Ref.auto(hf.entity.Bad.dropReward).replace(function(hf: hf.Hf, self: hf.entity.Bad) {
                return;
            }),

            Ref.auto(hf.entity.Bad.freeze).replace(function(hf: hf.Hf, self: hf.entity.Bad, timer: Float) {
                return;
            }),
        ];

        this.patches = FrozenArray.from(patches);
    }
}
